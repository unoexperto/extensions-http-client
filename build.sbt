import sbt.Keys._
import sbt._

lazy val commonSettings = Seq(
  name := "extensions-http-client",
  organization := "com.walkmind",
  version := "1.7",

  licenses := Seq("Apache-2.0" -> url("http://opensource.org/licenses/Apache-2.0")),
  scalacOptions := Seq(
    "-unchecked",
    "-deprecation",
    "-encoding", "utf8",
    "-language:implicitConversions",
    "-language:postfixOps",
    "-language:higherKinds",
    "-Xcheckinit"),

  scalaVersion := "2.12.9",
  crossScalaVersions := Seq("2.12.9", "2.13.0")
)

lazy val publishSettings = {
  Seq(
    bintrayOrganization := Some("cppexpert"),
    publishArtifact in Test := false,
    publishArtifact := true,

    scmInfo := Some(ScmInfo(url("https://gitlab.com/unoexperto/extensions-http-client.git"), "git@gitlab.com:unoexperto/extensions-http-client.git")),
    developers += Developer("unoexperto",
      "ruslan",
      "unoexperto.support@mailnull.com",
      url("https://gitlab.com/unoexperto")),
    pomIncludeRepository := (_ => false),
    bintrayPackage := "extensions-http-client"
  )
}

lazy val root = (project in file(".")).
  settings(commonSettings: _*).
  settings(publishSettings: _*).
  settings(
    libraryDependencies ++= {
      val catsVersion = "2.0.0-M4"
      Seq(
        "com.typesafe.akka" %% "akka-stream" % "2.6.0-M3",
        "com.typesafe.akka" %% "akka-http" % "10.1.8",

        "org.scala-lang.modules" %% "scala-collection-compat" % "2.1.1",

        "io.spray" %% "spray-json" % "1.3.5",
        "org.jsoup" % "jsoup" % "1.12.1",
        "org.asynchttpclient" % "async-http-client" % "2.10.1",

        "org.typelevel" %% "cats-core" % catsVersion,
        "org.typelevel" %% "cats-effect" % catsVersion
      )
    }
  )
  .enablePlugins(BintrayPlugin)